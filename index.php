<?php
session_start();
if(!empty($_SESSION['currency'])){
   $currency=$_SESSION['currency'];
}else{
   $currency='uah';
};


$currensies=[
   'uah'=>[
    'name' => 'Гривна',
    'course' => 1,
   ],
   'usd'=>[
      'name' => 'Доллар',
      'course' => 27.1,
   ],
   'eur'=> [
      'name' => 'Евро',
      'course' => 30.2,
   ]
];

// нумерованный массив с товарами
$products =[
   [
      'title' =>'Мягкая игрушка <Крокодил>',
      'price_val' =>'545',
   ],
   [
      'title' =>'Мягкая игрушка <Змея>',
      'price_val' =>'670',
   ],
   [
      'title' =>'Мягкая игрушка <Собака>',
      'price_val' =>'890',
   ],
   [
      'title' =>'Мягкая игрушка <Мишка>',
      'price_val' =>'1000',
   ],
   [
      'title' =>'Мягкая игрушка <Слон>',
      'price_val' =>'1100',
   ],
   [
      'title' =>'Мягкая игрушка <Заяц>',
      'price_val' =>'1150',
   ],
   [
      'title' =>'Мягкая игрушка <Панда>',
      'price_val' =>'1250',
   ],
   [
      'title' =>'Мягкая игрушка <Леопард>',
      'price_val' =>'1670',
   ],
   [
      'title' =>'Мягкая игрушка <Медведь>',
      'price_val' =>'1750',
   ], 
   [
      'title' =>'Мягкая игрушка <Тигр>',
      'price_val' =>'1900',
   ]
];
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="University database.">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
      <link rel="stylesheet" href="/css/style.css">
   </head> 
   <body>
      <div class="header">
         <div class="container-fluid">
           <h1>Products</h1>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <!-- определяем ключи в массиве валют -->
            <?php foreach($currensies as $key=>$rate):?>
               <!-- сравниваем ключи с переменной -->
               <?php if($currency == $key):?>
                  <!-- выводим весь товар с учетом выбраной валюты -->
                  <?php foreach($products as $product):?>
                     <div class="col-5">
                        <div class="card">
                           <div class="card-body">
                              <h4><?=$product['title']?></h4>
                              <p><?=$product['price_val']/$currensies[$key]['course'].' '.$currensies[$key]['name']?></p>
                           </div>
                        </div>
                     </div>
                  <?php endforeach;?>
               <?php endif;?>
            <?php endforeach;?>
            <div class="col-2">
               <!-- селект с выбором валют -->
              <form action="/one.php" method="post">
                  <div class="mb-3">
                    <label class="form-label" name="currency">Currency</label>
                    <select class="form-control" name="currency">
                       <option value="uah">Гривна</option>
                       <option value="usd">Доллар</option>
                       <option value="eur">Евро</option>
                     </select>
                  </div>
                  <div class="mb-3">
                    <button class="btn btn-primary">Отправить</button>
                  </div>
               </form>
            </div>
         </div>
      </div>  
      <div class="footer">
        <div class="text-center">
          <p class="footer-text">© 2021</p>
         </div>
      </div>
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
   </body>
</html>